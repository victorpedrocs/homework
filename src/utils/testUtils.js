const { Op } = require('sequelize');
const { Profile, Contract, Job } = require('../repo/model');
const { faker } = require('@faker-js/faker');

function buildProfile(profile) {
  return {
    firstName: faker.person.firstName(),
    lastName: faker.person.lastName(),
    profession: faker.person.jobTitle(),
    balance: faker.number.int({ min: 100 }),
    type: Profile.CLIENT_TYPE,
    ...profile,
  };
}

function buildContract(contract) {
  return {
    terms: faker.company.catchPhrase(),
    status: Contract.NEW_STATUS,
    ...contract,
  };
}

function buildJob(job) {
  return {
    description: faker.commerce.productDescription(),
    price: faker.number.float({ precision: 0.01, min: 100, max: 1000 }),
    ...job,
  };
}

async function createProfile(profile) {
  return Profile.create(buildProfile(profile));
}

async function createContract(contract) {
  return Contract.create(buildContract(contract));
}

async function createJob(job) {
  return Job.create(buildJob(job));
}

async function createPaidJob(job) {
  return Job.create(
    buildJob({
      paid: true,
      paymentDate: faker.date.recent(),
      ...job,
    }),
  );
}

async function createClientProfile(profile) {
  return createProfile({
    type: Profile.CLIENT_TYPE,
    ...profile,
  });
}

async function createContractorProfile(profile) {
  return createProfile({
    type: Profile.CONTRACTOR_TYPE,
    ...profile,
  });
}

async function deleteProfilesByIds(profileIds) {
  return Profile.destroy({ where: { id: { [Op.in]: profileIds } } });
}

async function deleteContractsByIds(contractIds) {
  return Contract.destroy({ where: { id: { [Op.in]: contractIds } } });
}

async function deleteJobsByIds(jobsIds) {
  return Job.destroy({ where: { id: { [Op.in]: jobsIds } } });
}

module.exports = {
  createProfile,
  createContract,
  createClientProfile,
  createContractorProfile,
  deleteProfilesByIds,
  deleteContractsByIds,
  createJob,
  createPaidJob,
  deleteJobsByIds,
};
