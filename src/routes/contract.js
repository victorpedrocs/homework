const { Router } = require('express');
const { Op } = require('sequelize');

const router = Router();

/**
 * Get a contract by id when the contract is related to the profile in request
 * @returns {Contract} contracts by id
 */
router.get('/:id', async (req, res) => {
  const { Contract } = req.app.get('models');
  const { id } = req.params;
  const profile = req.profile;

  req.log.info(
    { profileId: profile.id, contractId: id },
    'Getting a contract by id',
  );

  const contract = await Contract.findOne({
    where: {
      id,
      [Op.or]: [{ ContractorId: profile.id }, { ClientId: profile.id }],
    },
  });

  if (!contract) {
    req.log.info('contract not found');
    return res.status(404).end();
  }

  res.json(contract);
});

/**
 * Get a list of contracts by the profile id in request
 * @returns {Contract[]} contracts by id
 */
router.get('/', async (req, res) => {
  const { Contract } = req.app.get('models');
  const profile = req.profile;

  req.log.info({ profileId: profile.id }, 'Getting contracts by id');

  const contracts = await Contract.findAll({
    where: {
      [Op.or]: [{ ContractorId: profile.id }, { ClientId: profile.id }],
      status: {
        [Op.ne]: Contract.TERMINATED_STATUS,
      },
    },
  });

  if (!contracts) {
    req.log.info('contracts not found');
    return res.status(404).end();
  }

  req.log.info({ contracts }, 'contracts');

  res.json(contracts);
});

module.exports.contractRoutes = router;
