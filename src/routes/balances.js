const { Router } = require('express');
const { Op } = require('sequelize');

const router = Router();

/**
 * Make a deposit to a client profile
 * @returns {Profile} the updated client
 */
router.post('/deposit/:userId', async (req, res) => {
  const { Profile, Contract, Job } = req.app.get('models');
  const { userId } = req.params;
  const amount = Number(req.body.amount);

  req.log.info({ userId }, 'Making a deposit to the user');

  const profile = await Profile.findOne({
    where: {
      id: userId,
    },
  });

  if (!profile) {
    req.log.info('profile not found');
    return res.status(404).end();
  }

  if (!amount || amount < 1) {
    req.log.info('invalid amount');
    return res.status(400).end();
  }

  const jobsPriceSum = await Job.sum('price', {
    where: {
      paid: { [Op.not]: true },
    },
    include: {
      model: Contract,
      required: true,
      attributes: [],
      where: {
        ClientId: profile.id,
        // NOTE: I'm assuming active means the contract is in progress, and not new or terminated
        status: Contract.IN_PROGRESS_STATUS,
      },
    },
  });

  if (amount > jobsPriceSum * 0.25) {
    req.log.info('amount larger than allowed');
    return res.status(400).end();
  }

  const updatedProfile = await profile.update(
    { balance: profile.balance + amount },
    { returning: true },
  );

  res.json(updatedProfile);
});

module.exports.balancesRoutes = router;
