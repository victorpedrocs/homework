const { Router } = require('express');
const { Op } = require('sequelize');

const router = Router();

/**
 * Get a list of active unpaid jobs for a user, either contractor or client
 * @returns {Job[]} jobs by id
 */
router.get('/unpaid', async (req, res) => {
  const { Contract, Job } = req.app.get('models');
  const profile = req.profile;

  req.log.info({ profileId: profile.id }, 'Getting unpaid active jobs');

  const jobs = await Job.findAll({
    where: {
      paid: { [Op.not]: true },
    },
    include: {
      model: Contract,
      required: true,
      attributes: [],
      where: {
        [Op.or]: [{ ContractorId: profile.id }, { ClientId: profile.id }],
        // NOTE: I'm assuming active means the contract is in progress, and not new or terminated
        status: Contract.IN_PROGRESS_STATUS,
      },
    },
  });

  if (!jobs) {
    req.log.info('jobs not found');
    return res.status(404).end();
  }

  res.json(jobs);
});

router.get('/:id/pay', async (req, res) => {
  const { Contract, Job, Profile } = req.app.get('models');
  const profile = req.profile;
  const { id } = req.params;

  req.log.info({ profileId: profile.id, jobId: id }, 'Making payment');

  const job = await Job.findOne({
    where: {
      id,
      paid: { [Op.not]: true },
    },
    include: { model: Contract },
  });

  if (!job) {
    req.log.info('Job not found');
    return res.status(404).end();
  }

  if (profile.balance < job.price) {
    req.log.info('user has not enough balance');
    return res.status(400).end();
  }

  if (job.Contract.ClientId !== profile.id) {
    return res.status(400).end();
  }

  const contractor = await Profile.findOne({
    where: { id: job.Contract.ContractorId },
  });

  if (!contractor) {
    req.log.info({ job }, 'contractor not found');
    return res.status(400).end();
  }

  const newContractorBalance = contractor + job.price;
  const newClientBalance = profile.balance - job.price;

  try {
    const conn = req.app.get('sequelize');
    const updatedJob = await conn.transaction(async (transaction) => {
      await profile.update({ balance: newClientBalance }, { transaction });
      await contractor.update(
        { balanace: newContractorBalance },
        { transaction },
      );
      return await job.update(
        { paid: true, paymentDate: new Date().toISOString() },
        { transaction, returning: true },
      );
    });

    res.json(updatedJob);
  } catch (error) {
    req.log.error(error, 'failed to process payment');
    return res.status(500).end();
  }
});

module.exports.jobsRoutes = router;
