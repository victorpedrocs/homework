const { Router } = require('express');
const { Op, col, fn, literal } = require('sequelize');

const router = Router();

/**
 * Returns the profession that earned the most money (sum of jobs paid) for any contactor that worked in the query time range.
 * @returns {Profile} the updated client
 */
router.get('/best-profession', async (req, res) => {
  const { Profile, Contract, Job } = req.app.get('models');
  const { start, end } = req.query;

  req.log.info({ start, end }, 'Getting best professions');

  if (!start || !end || start > end) {
    req.log.info('invalid start or end');
    return req.status(400);
  }

  const job = await Job.findOne({
    attributes: [[fn('sum', col('price')), 'total'], 'paid', 'paymentDate'],
    where: {
      paymentDate: { [Op.between]: [start, end] },
    },
    include: {
      model: Contract,
      include: {
        model: Profile,
        association: 'Contractor',
      },
    },
    group: ['Contract.ContractorId'],
    order: literal('total DESC'),
  });

  if (!job) {
    req.log.info('no contractor found for these parameters');
    return res.status(404).send();
  }

  res.json(job.Contract.Contractor);
});

router.get('/best-clients', async (req, res) => {
  const { Profile, Contract, Job } = req.app.get('models');
  const { start, end } = req.query;
  const limit = req.query.limit || 2;

  req.log.info({ start, end, limit }, 'Getting best clients');

  if (!start || !end || start > end) {
    req.log.info('invalid start or end');
    return req.status(400);
  }

  const jobs = await Job.findAll({
    attributes: [[fn('sum', col('price')), 'total'], 'paid', 'paymentDate'],
    where: {
      paymentDate: { [Op.between]: [start, end] },
    },
    include: {
      model: Contract,
      include: {
        model: Profile,
        association: 'Client',
      },
    },
    group: ['Contract.ClientId'],
    order: literal('total DESC'),
    limit,
  });

  if (!jobs) {
    req.log.info('no client found for these parameters');
    return res.status(404).send();
  }

  res.json(jobs.map((job) => job.Contract.Client));
});

module.exports.adminRoutes = router;
