const request = require('supertest');
const {
  createClientProfile,
  createContractorProfile,
  createContract,
  deleteProfilesByIds,
  deleteContractsByIds,
} = require('../../utils/testUtils');
const app = require('../../app');
const { Contract } = require('../../repo/model');

describe('Route: /contracts', () => {
  let client, client2, contractor, contract, contract2;
  let contractIds = [],
    profileIds = [];

  beforeAll(async () => {
    client = await createClientProfile();
    client2 = await createClientProfile();
    contractor = await createContractorProfile();
    contract = await createContract({
      ClientId: client.id,
      ContractorId: contractor.id,
    });
    contract2 = await createContract({
      ClientId: client2.id,
      ContractorId: contractor.id,
    });

    profileIds.push(client.id, client2.id, contractor.id);
    contractIds.push(contract.id, contract2.id);
  });

  afterAll(async () => {
    await deleteProfilesByIds(profileIds);
    await deleteContractsByIds(contractIds);
  });

  describe('GET /contracts/:id', () => {
    it('should retrive a contract', async () => {
      const response = await request(app)
        .get('/contracts/' + contract.id)
        .set('profile_id', client.id)
        .expect(200);

      expect(response.body.id).toEqual(contract.id);
    });

    it('should not return a contract belonging to someone else', async () => {
      await request(app)
        .get('/contracts/' + contract2.id)
        .set('profile_id', client.id)
        .expect(404);
    });
  });

  describe('GET /contracts', () => {
    it('should retrive only profile contracts', async () => {
      const contract3 = await createContract({
        ClientId: client.id,
        Contractor: contractor.id,
      });
      const contractTerminated = await createContract({
        status: Contract.TERMINATED_STATUS,
        ClientId: client.id,
        Contractor: contractor.id,
      });
      contractIds.push(contract3.id, contractTerminated);

      const response = await request(app)
        .get('/contracts')
        .set('profile_id', client.id)
        .expect(200);

      const contracts = response.body;

      expect(contracts.length).toEqual(2);
      expect(
        contracts.find((c) => c.ClientId === client2.id),
      ).not.toBeDefined();
      expect(
        contracts.find((c) => c.status === Contract.TERMINATED_STATUS),
      ).not.toBeDefined();
    });
  });
});
