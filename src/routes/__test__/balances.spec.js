const request = require('supertest');
const {
  createClientProfile,
  createContractorProfile,
  createContract,
  deleteProfilesByIds,
  deleteContractsByIds,
  createJob,
  deleteJobsByIds,
} = require('../../utils/testUtils');
const app = require('../../app');
const { Contract } = require('../../repo/model');

describe('Route: /balances', () => {
  let maxDeposit = 0;
  let client, contractor, contract, job;
  let contractIds = [],
    profileIds = [],
    jobIds = [];

  beforeAll(async () => {
    client = await createClientProfile();
    contractor = await createContractorProfile();
    contract = await createContract({
      ClientId: client.id,
      ContractorId: contractor.id,
      status: Contract.IN_PROGRESS_STATUS,
    });
    job = await createJob({
      ContractId: contract.id,
    });
    const job2 = await createJob({
      ContractId: contract.id,
    });

    maxDeposit = (job.price + job2.price) * 0.25;

    profileIds.push(client.id, contractor.id);
    contractIds.push(contract.id);
    jobIds.push(job.id, job2.id);
  });

  afterAll(async () => {
    await deleteJobsByIds(jobIds);
    await deleteProfilesByIds(profileIds);
    await deleteContractsByIds(contractIds);
  });

  describe('POST /balances/deposit/:userId', () => {
    it('should successfuly deposit', async () => {
      await request(app)
        .post('/balances/deposit/' + client.id)
        .send({
          amount: maxDeposit,
        })
        .expect(200);
    });

    it('should fail to deposit value larger than allowed', async () => {
      await request(app)
        .post('/balances/deposit/' + client.id)
        .send({
          amount: maxDeposit + 1,
        })
        .expect(400);
    });
  });
});
