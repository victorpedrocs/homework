const request = require('supertest');
const {
  createClientProfile,
  createContractorProfile,
  createContract,
  deleteProfilesByIds,
  deleteContractsByIds,
  createJob,
  createPaidJob,
  deleteJobsByIds,
} = require('../../utils/testUtils');
const app = require('../../app');
const { Contract } = require('../../repo/model');

describe('Route: /contracts', () => {
  let client, contractor, contract, activeContract, job, paidJob, job2;
  let contractIds = [],
    profileIds = [],
    jobIds = [];

  beforeAll(async () => {
    client = await createClientProfile({
      balance: 500,
    });
    contractor = await createContractorProfile({
      balance: 200,
    });
    contract = await createContract({
      ClientId: client.id,
      ContractorId: contractor.id,
    });
    activeContract = await createContract({
      ClientId: client.id,
      ContractorId: contractor.id,
      status: Contract.IN_PROGRESS_STATUS,
    });
    job = await createJob({
      price: 50,
      ContractId: activeContract.id,
    });
    paidJob = await createPaidJob({
      ContractId: activeContract.id,
    });
    job2 = await createJob({
      ContractId: contract.id,
    });

    profileIds.push(client.id, contractor.id);
    contractIds.push(contract.id, activeContract.id);
    jobIds.push(job.id, paidJob.id, job2.id);
  });

  afterAll(async () => {
    await deleteJobsByIds(jobIds);
    await deleteContractsByIds(contractIds);
    await deleteProfilesByIds(profileIds);
  });

  describe('GET /jobs/unpaid', () => {
    it('should retrive a contract', async () => {
      const response = await request(app)
        .get('/jobs/unpaid')
        .set('profile_id', client.id)
        .expect(200);

      const jobs = response.body;

      expect(jobs).toHaveLength(1);
      expect(jobs[0].id).toEqual(job.id);
    });
  });

  describe('POST /jobs/:id/pay', () => {
    it('should successfully pay for the job', async () => {
      const response = await request(app)
        .get(`/jobs/${job.id}/pay`)
        .set('profile_id', client.id)
        .expect(200);

      const updatedJob = response.body;

      expect(updatedJob.paid).toBeTruthy();
    });

    it('should fail to pay without sufficient balance', async () => {
      const expensiveJob = await createJob({
        price: 1000,
        ContractId: activeContract.id,
      });
      jobIds.push(expensiveJob);
      await request(app)
        .get(`/jobs/${expensiveJob.id}/pay`)
        .set('profile_id', client.id)
        .expect(400);
    });
  });
});
