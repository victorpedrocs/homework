const request = require('supertest');
const {
  createClientProfile,
  createContractorProfile,
  createContract,
  deleteProfilesByIds,
  deleteContractsByIds,
  createJob,
  deleteJobsByIds,
} = require('../../utils/testUtils');
const app = require('../../app');
const { Contract } = require('../../repo/model');
const { faker } = require('@faker-js/faker');

describe('Route: /admin', () => {
  const queryStart = faker.date.recent({ days: 5 });
  const queryEnd = new Date();
  let client, client2, contractor, contractor2, contract, contract2;
  let contractIds = [],
    profileIds = [],
    jobIds = [];

  beforeAll(async () => {
    client = await createClientProfile();
    client2 = await createClientProfile();
    contractor = await createContractorProfile();
    contractor2 = await createContractorProfile();
    contract = await createContract({
      ClientId: client.id,
      ContractorId: contractor.id,
      status: Contract.TERMINATED_STATUS,
    });
    contract2 = await createContract({
      ClientId: client2.id,
      ContractorId: contractor2.id,
      status: Contract.TERMINATED_STATUS,
    });
    const job = await createJob({
      price: 500,
      paid: true,
      paymentDate: faker.date.between(queryStart, queryEnd),
      ContractId: contract.id,
    });
    const job2 = await createJob({
      price: 200,
      paid: true,
      paymentDate: faker.date.between(queryStart, queryEnd),
      ContractId: contract2.id,
    });

    profileIds.push(client.id, contractor.id, contractor2.id);
    contractIds.push(contract.id, contract2.id);
    jobIds.push(job.id, job2.id);
  });

  afterAll(async () => {
    await deleteJobsByIds(jobIds);
    await deleteProfilesByIds(profileIds);
    await deleteContractsByIds(contractIds);
  });

  describe('GET /admin/best-profession', () => {
    it('should get the correct profession', async () => {
      const result = await request(app)
        .get('/admin/best-profession')
        .query({
          start: queryStart.toISOString(),
          end: queryEnd.toISOString(),
        })
        .expect(200);

      const best = result.body;
      expect(best.id).toEqual(contractor.id);
    });

    it('should return nothing for empty range', async () => {
      await request(app)
        .get('/admin/best-profession')
        .query({
          start: faker.date
            .between(new Date(queryStart.getTime() - 100000), queryStart)
            .toISOString(),
          end: queryStart.toISOString(),
        })
        .expect(404);
    });
  });

  describe('GET /admin/best-clients', () => {
    it('should get the correct client', async () => {
      const result = await request(app)
        .get('/admin/best-clients')
        .query({
          start: queryStart.toISOString(),
          end: queryEnd.toISOString(),
          limit: 1,
        })
        .expect(200);

      const [best] = result.body;
      expect(best.id).toEqual(client.id);
    });

    it('should return nothing for empty range', async () => {
      const result = await request(app)
        .get('/admin/best-clients')
        .query({
          start: faker.date
            .between(new Date(queryStart.getTime() - 100000), queryStart)
            .toISOString(),
          end: queryStart.toISOString(),
        })
        .expect(200);

      expect(result.body).toHaveLength(0);
    });
  });
});
