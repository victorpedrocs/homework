const { Router } = require('express');
const { getProfile } = require('../middleware/getProfile');
const { contractRoutes } = require('./contract');
const { jobsRoutes } = require('./jobs');
const { balancesRoutes } = require('./balances');
const { adminRoutes } = require('./admin');

const router = Router();

router.use('/contracts', getProfile, contractRoutes);
router.use('/jobs', getProfile, jobsRoutes);
router.use('/balances', balancesRoutes);
router.use('/admin', adminRoutes);

module.exports.router = router;
