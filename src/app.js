const express = require('express');
const bodyParser = require('body-parser');
const { sequelize } = require('./repo/model');
const pino = require('pino-http');
const { router } = require('./routes');

const app = express();

app.use(bodyParser.json());
app.use(pino());

app.set('sequelize', sequelize);
app.set('models', sequelize.models);

// attach routes to server
app.use(router);

module.exports = app;
